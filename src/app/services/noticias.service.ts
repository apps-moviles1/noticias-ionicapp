import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaTopHeadlines } from '../interfaces/interfaces';
import { environment } from 'src/environments/environment';

const API_KEY = environment.apiKey;
const API_URL = environment.apiUrl;
const API_LANGUAGE = environment.apiLanguage;

const headers = new HttpHeaders({
  'X-Api-key': API_KEY
})

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  headlinesPage = 0;

  categoriaActual = '';
  categoriaPage = 0;

  constructor( private http: HttpClient ) { }
  
  private ejecutarQuery<T>( query: string ){
    
    query = API_URL + query;

    return this.http.get<T>( query, { headers } );

  }

  getTopHeadlines(){

    this.headlinesPage++;
    
    return this.ejecutarQuery<RespuestaTopHeadlines>(`/top-headlines?country=${API_LANGUAGE}&page=${ this.headlinesPage }`);
    //return this.http.get<RespuestaTopHeadlines>(`http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=228465cecd0241b8ad92f4b6f708da61`)

  }

  getTopHeadlinesCategoria( categoria: string ){

    if( this.categoriaActual === categoria ){
      this.categoriaPage++;
    }else{
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }

    return this.ejecutarQuery<RespuestaTopHeadlines>(`/top-headlines?country=${API_LANGUAGE}&category=${ categoria }&page=${ this.categoriaPage }`)
    //return this.http.get<RespuestaTopHeadlines>(`http://newsapi.org/v2/top-headlines?country=de&category=business&apiKey=228465cecd0241b8ad92f4b6f708da61`)
    
  }

}
